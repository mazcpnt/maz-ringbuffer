# 图形化显示缓冲池信息

## 编译

```
paul@vmware:~/gitee/maz-ringbuffer/examples/print-info$ make
gcc -I./ -I../../ -I../../portable/linux -lpthread -c main.c -o main.o
gcc -I./ -I../../ -I../../portable/linux -lpthread -c ../../ringbuffer.c -o ../../ringbuffer.o
gcc main.o ../../ringbuffer.o ../../ringbuffer_ex.o -I./ -I../../ -I../../portable/linux -lpthread -o app
paul@vmware:~/gitee/maz-ringbuffer/examples/print-info$ 
```

## 执行

```
paul@vmware:~/gitee/maz-ringbuffer/examples/print-info$ ./app 20 0 100
          W                                          WIDX = 20
----------+---------------------------------------   RIDX = 0
XXXXXXXXXX                                           SIZE = 100
+-------------------------------------------------   IDLE = 79
R                                                    FILL = 20
paul@vmware:~/gitee/maz-ringbuffer/examples/print-info$ ./app 40 0 100
                    W                                WIDX = 40
--------------------+-----------------------------   RIDX = 0
XXXXXXXXXXXXXXXXXXXX                                 SIZE = 100
+-------------------------------------------------   IDLE = 59
R                                                    FILL = 40
paul@vmware:~/gitee/maz-ringbuffer/examples/print-info$ ./app 40 20 100
                    W                                WIDX = 40
--------------------+-----------------------------   RIDX = 20
          XXXXXXXXXX                                 SIZE = 100
----------+---------------------------------------   IDLE = 79
          R                                          FILL = 20
paul@vmware:~/gitee/maz-ringbuffer/examples/print-info$ ./app 40 40 100
                    W                                WIDX = 40
--------------------+-----------------------------   RIDX = 40
                                                     SIZE = 100
--------------------+-----------------------------   IDLE = 99
                    R                                FILL = 0
paul@vmware:~/gitee/maz-ringbuffer/examples/print-info$ ./app 20 40 100
          W                                          WIDX = 20
----------+---------------------------------------   RIDX = 40
XXXXXXXXXX          XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX   SIZE = 100
--------------------+-----------------------------   IDLE = 19
                    R                                FILL = 80
paul@vmware:~/gitee/maz-ringbuffer/examples/print-info$ 
```

