#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <ringbuffer_ex.h>

int main(int argc, char **argv)
{
    int w = 0;
    int r = 0;
    int size = 0;
    RINGBUFFER rbuf = { 0 };

    /* 打印函数使用方法 */
    if (argc != 4)
    {
        printf("usage: %s <w> <r> <size> \n", argv[0]);
        printf("eg   : %s 30 60 100\n", argv[0]);
        printf("eg   : %s 60 60 100\n", argv[0]);
        printf("eg   : %s 60 30 100\n", argv[0]);
        printf("\n");
        return -1;
    }

    sscanf(argv[1], "%d", &w);
    sscanf(argv[2], "%d", &r);
    sscanf(argv[3], "%d", &size);

    rbuf.w = w;
    rbuf.r = r;
    rbuf.size = size;
    ringbuffer_status(&rbuf);

    return 0;
}

