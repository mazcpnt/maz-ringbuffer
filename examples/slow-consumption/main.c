#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <ringbuffer_ex.h>

#define DEMO_DATA_SIZE 24

int main(int argc, char **argv)
{
    int i = 0;
    int j = 0;
    int ret = 0;
    RINGBUFFER rbuf = { 0 };
    HEADER hd;
    uint8_t *ibuf = NULL;
    uint8_t *obuf = NULL;

    ibuf = malloc(DEMO_DATA_SIZE);
    obuf = malloc(DEMO_DATA_SIZE);

    /* 准备好填充数据 */
    for(j = 0; j < DEMO_DATA_SIZE; j++)
    {
        ibuf[j] = j + 'A';
    }

    printf("sizeof(HEADER) = %ld\n", sizeof(HEADER));

    ret = ringbuffer_init(&rbuf, 256);
    if(ret)
        printf("err: ringbuffer_init\n");
    else
        printf("success: ringbuffer_init\n");

    for(i = 0; i < 200; i++)
    {
        memset(&hd, 0, sizeof(HEADER));
        hd.sync[0] = SYNC0;
        hd.sync[1] = SYNC1;
        hd.sync[2] = SYNC2;
        hd.sync[3] = SYNC3;
        hd.len = DEMO_DATA_SIZE;
        ret = ringbuffer_write_with_header(&rbuf, (uint8_t *)&hd, sizeof(HEADER), ibuf, DEMO_DATA_SIZE);
        if(ret)
            printf("err: ringbuffer_write_with_header\n");
        else
            printf("success: ringbuffer_write_with_header\n");

        printf("= WRITE ================================================================================================================================= %03d ==\n", i);
        ringbuffer_print(&rbuf);
        printf("= WRITE ================================================================================================================================= %03d ==\n", i);

        memset(&hd, 0, sizeof(HEADER));
        ret = ringbuffer_read_frame(&rbuf, &hd, obuf);
        if(ret)
            printf("err: ringbuffer_read_frame\n");
        else
            printf("success: read frame, len = %d\n", hd.len);
    }

    ret = ringbuffer_deinit(&rbuf);
    if(ret) printf("err: ringbuffer_deinit\n");
    else printf("success: ringbuffer_deinit\n");

    return 0;
}

