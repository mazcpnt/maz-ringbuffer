/*
 * ringbuffer.h
 *
 *  Created on: 2021年6月6日
 *      Author: wangbing
 *      Email : mz8023yt@163.com
 */

#ifdef __cplusplus
extern "C"
{
#endif

#ifndef _RINGBUFFER_EX_H_
#define _RINGBUFFER_EX_H_

#include <stdint.h>
#include <ringbuffer.h>

/**
 * @brief 示例同步码
 */
#define SYNC0   'M'
#define SYNC1   'A'
#define SYNC2   'Z'
#define SYNC3   '-'

/**
 * @brief 数据头
 */
typedef struct _HEADER_
{
    uint8_t sync[4];                // 同步码
    uint32_t len;
} HEADER;

/**
 * @brief API接口
 */
int ringbuffer_read_frame(RINGBUFFER *rbuf, HEADER *hbuf, uint8_t *obuf);

#endif /* _RINGBUFFER_EX_H_ */

#ifdef __cplusplus
}
#endif

