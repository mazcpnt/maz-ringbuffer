/*
 * ringbuffer_config.h
 *
 *  Created on: 2021年6月6日
 *      Author: wangbing
 *      Email : mz8023yt@163.com
 */

#ifdef __cplusplus
extern "C"
{
#endif

#ifndef _RINGBUFFER_CONFIG_H_
#define _RINGBUFFER_CONFIG_H_

/**
 * 丢帧方式
 *      0: 丢弃最老的帧, 读取时需要给读指针上锁
 *      1: 丢弃最新的帧, 第一次取数据需要先清空QUEUE
 */
#define RINGBUFFER_LOST_OLDEST      0
#define RINGBUFFER_LOST_LATEST      1
#define RINGBUFFER_LOST_WAY         RINGBUFFER_LOST_OLDEST

#endif /* _RINGBUFFER_CONFIG_H_ */

#ifdef __cplusplus
}
#endif

